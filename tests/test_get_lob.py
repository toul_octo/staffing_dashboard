from unittest import TestCase

from src.get_lob import get_lobs_ids_in_league

example_lob_json = [
    {'id': 184, 'abbreviation': 'STAGE', 'url': 'https://octopod.octo.com/api/v0/lobs/184', 'name': 'Stage',
     'turnover_type': 'from_internal_team', 'active': True, 'in_activity_rate': False,
     'timesheet_email_alert_enabled': False, 'created_at': '2019-03-15T15:40:23Z',
     'updated_at': '2019-03-15T15:40:23Z',
     'subsidiary': {'id': 1, 'name': 'OCTO France', 'region': 'fr', 'chrono_prefix': 'FR', 'locale': 'fr',
                    'timezone': 'Europe/Paris', 'active': True,
                    'url': 'https://octopod.octo.com/api/v0/subsidiaries/1', 'currency': {'symbol': '€'}},
     'league': {'id': 1, 'name': 'Default'}},
    {'id': 181, 'abbreviation': 'AIDA', 'url': 'https://octopod.octo.com/api/v0/lobs/181',
     'name': 'Applied Intelligence Datascience et Advisory', 'turnover_type': 'from_internal_team',
     'active': True,
     'in_activity_rate': True, 'timesheet_email_alert_enabled': True, 'created_at': '2019-01-09T16:30:38Z',
     'updated_at': '2019-11-12T14:51:17Z',
     'subsidiary': {'id': 1, 'name': 'OCTO France', 'region': 'fr', 'chrono_prefix': 'FR', 'locale': 'fr',
                    'timezone': 'Europe/Paris', 'active': True,
                    'url': 'https://octopod.octo.com/api/v0/subsidiaries/1', 'currency': {'symbol': '€'}},
     'league': {'id': 2, 'name': 'Science@Scale'}}]


class Test(TestCase):
    def test_get_ids_from_dict_of_lobs(self):
        # Given
        expected_lob_ids = [181]
        # When
        lobs_ids = get_lobs_ids_in_league(example_lob_json)

        # Then
        self.assertListEqual(lobs_ids, expected_lob_ids)
