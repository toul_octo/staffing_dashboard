from unittest import TestCase

from src.api_utils import compute_pages_to_parse


class Test(TestCase):
    def test_compute_pages_to_parse(self):
        # Given
        total_result = 706
        expected_pages_to_parse = [2, 3, 4, 5, 6, 7, 8]
        # When
        pages_to_parse = compute_pages_to_parse(total_result=total_result, max_result_per_page=100)

        # Than
        self.assertListEqual(pages_to_parse, expected_pages_to_parse)
