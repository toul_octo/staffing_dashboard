from unittest import TestCase

from src.get_people import get_people_in_lobs

example_people = [{'id': 1, 'last_name': 'Last_name', 'first_name': 'First Name', 'nickname': 'LMFN',
                   'url': 'https://octopod.octo.com/api/v0/people/1', 'email': 'f.last_name@octo.com',
                   'job': {'id': 0, 'name': 'Other', 'url': 'https://octopod.octo.com/api/v0/jobs/0'},
                   'lob': {'id': 244, 'abbreviation': 'NL', 'url': 'https://octopod.octo.com/api/v0/lobs/244'},
                   'included_in_activity_rate': False, 'created_at': '2020-01-13T15:24:51Z',
                   'updated_at': '2020-01-20T14:14:05Z',
                   'entry_date': '2020-01-01', 'leaving_date': None},
                  {'id': 2, 'last_name': 'Dylan', 'first_name': 'Bob', 'nickname': 'BODY',
                   'url': 'https://octopod.octo.com/api/v0/people/2', 'email': 'b.dylan@octo.com',
                   'job': {'id': 0, 'name': 'Other', 'url': 'https://octopod.octo.com/api/v0/jobs/0'},
                   'lob': {'id': 1, 'abbreviation': 'NL', 'url': 'https://octopod.octo.com/api/v0/lobs/1'},
                   'included_in_activity_rate': False, 'created_at': '2020-01-13T15:24:51Z',
                   'updated_at': '2020-01-20T14:14:05Z',
                   'entry_date': '2020-01-01', 'leaving_date': None}
                  ]


class Test(TestCase):
    def test_get_people_in_lobs(self):
        # Given
        lobs_ids = [1]
        expected_people_ids = [2]

        # When
        people_ids = get_people_in_lobs(example_people, lobs_ids=lobs_ids)

        # Then
        self.assertListEqual(expected_people_ids, people_ids)
