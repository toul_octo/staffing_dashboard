from unittest import TestCase, mock

from src.api_utils import get_all_pages

MAX_RESULT_PER_PAGE = 2
NUMBER_OF_PAGES_TO_CRAWL = 3
NUMBER_OF_RESULT = MAX_RESULT_PER_PAGE * NUMBER_OF_PAGES_TO_CRAWL
SOME_RESULT = ["a", "list"]


class MockResult:
    def __init__(self):
        self.headers = {"total": NUMBER_OF_RESULT}

    def json(self):
        return SOME_RESULT


def one_page_call_function():
    pass


class Test(TestCase):
    @mock.patch("tests.test_api_utils.one_page_call_function", return_value=MockResult())
    def test_get_all_pages(self, mock_one_page_call_function):
        # Given
        expected_result = SOME_RESULT * NUMBER_OF_PAGES_TO_CRAWL

        # When
        result = get_all_pages(mock_one_page_call_function, max_result_per_page=MAX_RESULT_PER_PAGE)

        # Then
        self.assertEqual(mock_one_page_call_function.call_count, NUMBER_OF_PAGES_TO_CRAWL)
        self.assertListEqual(expected_result, result)
