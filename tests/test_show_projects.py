from unittest import TestCase

from src.show_projects import api_url_to_web_url, transform_project_to_write_to_google_api


class Test(TestCase):
    def test_api_url_to_web_url(self):
        # Given
        url = "https://octopod.octo.com/api/v0/projects/2146908289"
        expected_url = "https://octopod.octo.com/projects/2146908289"

        # When
        web_url = api_url_to_web_url(url)

        # Then
        self.assertEqual(web_url, expected_url)

    def test_transform_project_to_write_to_google_api(self):
        # Given
        projects = {"status1": [{"project_name": "aa",
                                 "project_url": "url",
                                 "customer_name": "customer"},
                                {"project_name": "bb",
                                 "project_url": "url",
                                 "customer_name": "customer"}
                                ],
                    "status2": [{"project_name": "cc",
                                 "project_url": "url",
                                 "customer_name": "customer"}]
                    }

        expected_result = [["Status1 (2)", "Project Name", "URL", "Customer Name"],
                           ["", "aa", "url", "customer"],
                           ["", "bb", "url", "customer"],
                           [],
                           ["Status2 (1)", "Project Name", "URL", "Customer Name"],
                           ["", "cc", "url", "customer"],
                           []]
        # When
        result = transform_project_to_write_to_google_api(projects)

        # Then
        self.assertListEqual(result, expected_result)
