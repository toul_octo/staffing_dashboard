from setuptools import setup

setup(
    name='staffing_dashboard',
    version='0.1',
    packages=['src'],
    url='',
    license='',
    author='TOUL',
    author_email='',
    description='',
    install_requires=[
        "streamlit",
        # Google api
        "google-api-python-client",
        "google-auth-httplib2",
        "google-auth-oauthlib"

    ]
)
