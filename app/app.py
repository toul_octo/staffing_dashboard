import streamlit as st

from src.show_projects import load_activities, get_project_and_status, clean_status

activities = load_activities('/home/eltoulemonde/Documents/octo/staffing_dashboard/data/activities_in_league.json')
projects = get_project_and_status(activities)

st.sidebar.text("Cercle staffing Science@Scale")
for project_status in projects.keys():
    st.header("{project_status:} ({number_of_project:})".format(project_status=clean_status(project_status),
                                                                number_of_project=len(projects[project_status])))
    for project in projects[project_status]:
        st.markdown("{project_name:} for {client:} ([octopod]({link:}))".format(project_name=project["project_name"],
                                                                                client=project["customer_name"],
                                                                                link=project["project_url"]))
