import os
from datetime import datetime

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from src.get_activities import get_and_save_activities
from src.show_projects import load_clean_and_write_activities

ENV_TYPE = os.environ.get('ENV_TYPE')

default_args = {
    "retries": 0,
    "task_concurrency": 2,
    "depends_on_past": False
}

PATH_TO_DATA = os.getenv('PATH_TO_DATA', "../data/")

dag = DAG("cercle_satffing_parse_octopod_and_write_activities",
          description="Get all activities from octo pod and write them in google",
          default_args=default_args,
          schedule_interval="15 * * * MON-FRI",
          start_date=datetime(2020, 2, 11, 16, 14, 00))

get_and_save_activities_task = PythonOperator(task_id="get_and_save_activities",
                                              python_callable=get_and_save_activities,
                                              op_kwargs={"path_to_write": os.path.join(PATH_TO_DATA,
                                                                                       "activities_in_league.json")},
                                              dag=dag)

load_clean_and_write_activities_task = PythonOperator(task_id="load_clean_and_write_activities",
                                                      python_callable=load_clean_and_write_activities,
                                                      op_kwargs={"path_to_read": os.path.join(PATH_TO_DATA,
                                                                                              "activities_in_league.json")},
                                                      dag=dag)

get_and_save_activities_task >> load_clean_and_write_activities_task
