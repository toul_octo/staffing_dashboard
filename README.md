Credentials Google 
==================

To get google api credentials :
https://developers.google.com/sheets/api/quickstart/python

Then download it in file gsheet_credentials.json.


Sur l'EC 2
===========
# Setup  docker:

```
sudo apt-get update

sudo apt-get install -y \
    awscli \
    socat \
    apt-transport-https \
    ca-certificates \
    curl wget \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker ubuntu

sudo curl -L "https://github.com/docker/compose/releases/download/1.25.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```


First run
```
docker network create docker.network
docker-compose up postgres
docker-compose up initdb
docker-compose up scheduler webserver worker
```

Next runs
```
docker-compose up
```


## Google authorization
To get an authorization from google, one must visit the url and accept.

It will then throw you to a localhost adress and fail.

Solution come from here : https://stackoverflow.com/questions/46693297/how-to-authenticate-with-googlesheets-package-in-a-head-less-environmentec2-ins

Go to EC2 then
```
docker exec -it <scheduler_name> bash
```

In container launch python and
```
import requests
requests.get(<insert local url>)
```