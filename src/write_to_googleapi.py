from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file gsheet_token.pickle.
from src.get_token import PATH_TO_CREDS

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1vY9legLwkIR-1y-OIp7jWkwDtaoXZVpVwJeLafognn4'
JOBS_RANGE = 'Projets/Leads!A1:Z'


def clear_sheet(range_name: str):
    write_to_gsheet([[""] * 26] * 10000, range_name=range_name)


def write_to_gsheet(values_to_write: list, range_name):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = get_google_api_credentials()

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    sheet.values().update(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                          range=range_name,
                          valueInputOption="RAW",
                          body={"values": values_to_write}).execute()


def get_google_api_credentials():
    creds = None
    # The file gsheet_token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(os.path.join(PATH_TO_CREDS, 'gsheet_token.pickle')):
        with open(os.path.join(PATH_TO_CREDS, 'gsheet_token.pickle'), 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                os.path.join(PATH_TO_CREDS, 'gsheet_credentials.json'), SCOPES)
            authorization_url, state = flow.authorization_url(
                # Enable offline access so that you can refresh an access token without
                # re-prompting the user for permission. Recommended for web server apps.
                access_type='offline',
                # Enable incremental authorization. Recommended as a best practice.
                include_granted_scopes='true')
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(os.path.join(PATH_TO_CREDS, 'gsheet_token.pickle'), 'wb') as token:
            pickle.dump(creds, token)
    return creds


if __name__ == '__main__':
    write_to_gsheet(values_to_write=[[1, 2, 3], [4, 5, 6]], range_name=JOBS_RANGE)
