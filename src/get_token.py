import json
import logging
import os

import requests

PATH_TO_CREDS = os.getenv('PATH_TO_CREDS', "../creds/")
logging.getLogger().setLevel("INFO")


def get_octopod_token():
    logging.info("Ask for an octopod token")
    headers = {'Content-Type': 'application/x-www-form-urlencoded',
               "Accept": "application / json"
               }
    with open(os.path.join(PATH_TO_CREDS, "octopod_api_cred.json"), "r") as json_file:
        octopod_creds = json.load(json_file)

    r = requests.post('https://octopod.octo.com/api/oauth/token', headers=headers,
                      data={"grant_type": "client_credentials",
                            "client_id": octopod_creds["CLIENT_ID"],
                            "client_secret": octopod_creds["CLIENT_SECRET"]})
    if r.status_code == 200:
        return r.json()["access_token"]
    else:
        raise ValueError("Got status code {}".format(r.status_code))


if __name__ == "__main__":
    print(get_octopod_token())
