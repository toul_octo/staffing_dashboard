import requests

from src.get_token import get_octopod_token


def get_lobs_json_from_octopod(token: str):
    headers = {"Accept": "application / json",
               "Authorization": "Bearer " + token}
    r = requests.get('https://octopod.octo.com/api/v0/lobs?order_by=id&order=desc&page=1', headers=headers)

    return r.json()


def get_lobs_ids_in_league(lobs_json: dict, league_name='Science@Scale'):
    lobs_ids = []
    for lob in lobs_json:
        lob_id = lob["id"]
        lob_league_name = lob["league"]["name"]
        if lob_league_name == league_name:
            lobs_ids.append(lob_id)
    return lobs_ids


def get_lobs_in_league(token: str, league_name: str) -> list:
    lobs_json = get_lobs_json_from_octopod(token)
    return get_lobs_ids_in_league(lobs_json, league_name=league_name)


if __name__ == "__main__":
    token = get_octopod_token()
    print(get_lobs_in_league(token=token, league_name="Science@Scale"))
