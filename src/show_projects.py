import json
from collections import defaultdict

from src.write_to_googleapi import write_to_gsheet, clear_sheet, JOBS_RANGE


def load_activities(path):
    with open(path, "r") as json_file:
        data = json.load(json_file)
    return data


def activity_has_project(activity):
    return activity["activity"]["project"] is not None


def get_project_and_status(activities: list):
    project_by_status = defaultdict(list)
    for activity in activities:
        if activity_has_project(activity):
            project_name = get_project_name(activity)
            project_url = api_url_to_web_url(get_project_url(activity))
            project_status = get_project_status(activity)
            customer_name = get_project_client(activity)

            project_info = {"project_name": project_name,
                            "project_url": project_url,
                            "customer_name": customer_name}
            if project_info not in project_by_status[project_status]:
                project_by_status[project_status].append(project_info)

    return project_by_status


def get_project_status(activity):
    if activity["activity"]["project"]["status"]:
        return activity["activity"]["project"]["status"]
    else:
        return "Internal project"


def get_project_url(activity):
    return activity["activity"]["project"]["url"]


def get_project_name(activity):
    return activity["activity"]["project"]["name"]


def get_project_client(activity):
    if activity["activity"]["project"]["customer"] is not None:
        return activity["activity"]["project"]["customer"]["name"]
    else:
        return "No customer"


def api_url_to_web_url(url: str) -> str:
    return url.replace("/api/v0", "")


def transform_project_to_write_to_google_api(projects: dict):
    result = [["Ci-dessous sont écrit l'ensemble des projets sur lesquels des Octo de la ligue S@S ont tacé dans les 30 derniers / 30 prochains jours."],
              ["Ces projets sont extraits automatiquement de Octopod. Pour contribuer : https://gitlab.com/etoulemonde/staffing_dashboard"]]
    for project_status in sorted(projects.keys()):
        result.append(["{project_status:} ({number_of_project:})".format(project_status=clean_status(project_status),
                                                                         number_of_project=len(
                                                                             projects[project_status])),
                       "Project Name",
                       "URL",
                       "Customer Name"])
        for project in projects[project_status]:
            result.append(["", project["project_name"], project["project_url"], project["customer_name"]])
        result.append([])
    return result


def clean_status(status):
    if status is not None:
        return status.replace("_", " ").title()
    else:
        return "Internal Projects"


def load_clean_and_write_activities(path_to_read: str):
    activities = load_activities(path_to_read)
    projects_by_status = get_project_and_status(activities)
    google_style_project = transform_project_to_write_to_google_api(projects_by_status)
    clear_sheet(JOBS_RANGE)
    write_to_gsheet(google_style_project, range_name=JOBS_RANGE)


if __name__ == "__main__":
    load_clean_and_write_activities(path_to_read='../data/activities_in_league.json')
