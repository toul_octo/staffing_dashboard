import logging

import requests

from src.get_lob import get_lobs_in_league
from src.get_token import get_octopod_token


def get_peoples_from_octopod(token):
    headers = {"Accept": "application / json",
               "Authorization": "Bearer " + token}
    r = requests.get('https://octopod.octo.com/api/v0/people?order_by=id&order=desc&all=false', headers=headers)

    return r.json()


def get_people_in_lobs(people_json, lobs_ids: list) -> list:
    people_ids = []
    for people in people_json:
        people_id = people["id"]
        lob_id = people["lob"]["id"]
        if lob_id in lobs_ids:
            people_ids.append(people_id)
    return people_ids


def get_league_people_ids(token: str, league_name: str):
    lobs_ids_in_league = get_lobs_in_league(token=token, league_name=league_name)
    people_json = get_peoples_from_octopod(token)
    people_in_lob = get_people_in_lobs(people_json=people_json, lobs_ids=lobs_ids_in_league)
    logging.info("Found {} peoples in league {}".format(len(people_in_lob), league_name))
    return people_in_lob


if __name__ == "__main__":
    token = get_octopod_token()
    print(get_league_people_ids(token, league_name="Science@Scale"))
