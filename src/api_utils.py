import math


def get_all_pages(one_page_call_function, max_result_per_page: int, **kwargs):
    all_results = []
    r = one_page_call_function(page_number=1, per_page=max_result_per_page, **kwargs)
    all_results.extend(r.json())
    total_result = int(r.headers["total"])
    pages_left_to_parse = compute_pages_to_parse(total_result, max_result_per_page=max_result_per_page)
    for page_number in pages_left_to_parse:
        r = one_page_call_function(page_number=page_number, per_page=max_result_per_page, **kwargs)
        all_results.extend(r.json())
    return all_results


def compute_pages_to_parse(total_result: int, max_result_per_page: int) -> list:
    n_pages = math.ceil(total_result / max_result_per_page)
    pages_left_to_parse = [p for p in range(2, n_pages + 1)]
    return pages_left_to_parse
