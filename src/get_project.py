import logging
from datetime import datetime

import requests

from src.api_utils import get_all_pages, API_MAX_PROJECT_PER_PAGES
from src.get_activities import is_project_handled_by_list_of_people
from src.get_people import get_league_people_ids
from src.get_token import get_octopod_token

MAX_PROJECTS_PER_PAGE = 100

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


def get_not_ended_projects_from_octopod(token: str) -> list:
    all_results = get_all_pages(get_one_page_of_not_ended_projects_from_octopod,
                                max_result_per_page=MAX_PROJECTS_PER_PAGE,
                                token=token)
    logging.info("Found {} projects not ended".format(len(all_results)))
    return all_results


def get_one_page_of_not_ended_projects_from_octopod(page_number, token):
    headers = {"Accept": "application / json",
               "Authorization": "Bearer " + token}
    r = requests.get(
        'https://octopod.octo.com/api/v0/projects?end_date_after={date:}&order_by=created_at&order=desc&page={page:}&per_page={max_per_page:}'.format(
            date=datetime.now().strftime(format="%Y-%m-%d"),
            page=page_number,
            max_per_page=API_MAX_PROJECT_PER_PAGES), headers=headers)
    return r


def reduce_list_of_projects_to_projects_with_searched_people(projects_json, people_ids: list, token: str):
    reduced_list_of_projects = []
    for project in projects_json:
        if is_project_handled_by_list_of_people(token=token, project_id=project["id"], list_of_people_ids=people_ids):
            reduced_list_of_projects.append(project)
    return reduced_list_of_projects


def get_projects_in_league(token: str, league_name: str) -> list:
    people_ids = get_league_people_ids(token=token, league_name=league_name)
    projects_json = get_not_ended_projects_from_octopod(token=token)
    projects_in_league = reduce_list_of_projects_to_projects_with_searched_people(projects_json=projects_json,
                                                                                  people_ids=people_ids,
                                                                                  token=token)
    return projects_in_league


if __name__ == "__main__":
    token = get_octopod_token()
    print("Number of active project in league", len(get_projects_in_league(token=token, league_name="Science@Scale")))
