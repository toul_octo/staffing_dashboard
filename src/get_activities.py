import logging
import os
from datetime import datetime, timedelta

import requests

from src.api_utils import get_all_pages
from src.get_lob import get_lobs_in_league
from src.get_token import get_octopod_token

MAX_ACTIVITIES_PER_PAGE = 500

logging.getLogger().setLevel("INFO")


def get_activities_from_octopod(token: str, project_id: str):
    headers = {"Accept": "application / json",
               "Authorization": "Bearer " + token}
    r = requests.get('https://octopod.octo.com/api/v0/projects/{project_id:}/time_input'.format(project_id=project_id),
                     headers=headers)

    return r.json()


def is_project_handled_by_list_of_people(token: str, project_id: str, list_of_people_ids: list) -> bool:
    activities = get_activities_from_octopod(token=token, project_id=project_id)
    for activity in activities:
        if activity["person"]["id"] in list_of_people_ids:
            return True
    return False


def get_one_page_of_activities_in_lob(token, lob_id, start_date, end_date, page_number, per_page: int):
    headers = {"Accept": "application / json",
               "Authorization": "Bearer " + token}
    r = requests.get(
        'https://octopod.octo.com/api/v0/lobs/{lob_id:}/time_input?from_date={start_date:}&to_date={end_date:}&page={page_number:}&per_page={per_page:}'.format(
            lob_id=lob_id, start_date=start_date, end_date=end_date, page_number=page_number, per_page=per_page),
        headers=headers)

    return r


def get_all_activities_in_lob(token, lob_id, start_date, end_date):
    return get_all_pages(get_one_page_of_activities_in_lob, max_result_per_page=MAX_ACTIVITIES_PER_PAGE,
                         token=token, lob_id=lob_id, start_date=start_date,
                         end_date=end_date)


def get_all_activities_in_league(token, league_name, start_date, end_date):
    lobs_in_league = get_lobs_in_league(token=token, league_name=league_name)
    results = []
    for lob in lobs_in_league:
        activities_this_lob = get_all_activities_in_lob(token=token, lob_id=lob, start_date=start_date,
                                                        end_date=end_date)
        logging.info("Found {} activities for lob {}".format(len(activities_this_lob), lob))
        results.extend(activities_this_lob)
    return results


def get_and_save_activities(path_to_write: str):
    token = get_octopod_token()
    activities_in_league = get_all_activities_in_league(token=token, league_name="Science@Scale",
                                                        start_date=datetime.now() - timedelta(days=30),
                                                        end_date=datetime.now() + timedelta(days=30))
    print(len(activities_in_league))
    import json
    if not os.path.isdir(os.path.dirname(path_to_write)):
        os.makedirs(os.path.dirname(path_to_write))
    with open(path_to_write, 'w+') as outfile:
        logging.info("Saving to {}".format(path_to_write))
        json.dump(activities_in_league, outfile)
        logging.info("Saved to {}".format(path_to_write))
        logging.info("In path : {}".format(os.listdir(os.path.dirname(path_to_write))))

if __name__ == "__main__":
    get_and_save_activities(path_to_write='../data/activities_in_league.json')
